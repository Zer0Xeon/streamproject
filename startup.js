// Startup Sequence
// Constants
const chalk = require('chalk');
const log = console.log;

log(chalk.blue(`SP Running on`));
log(chalk.green(`Arch: ${process.arch}`));
log(chalk.yellow(`Platform: ${process.platform}`));
log(chalk.red(`Terminal: ${process.env.SHELL}`));