# Zero's Stream Project

## What is this?

Simply put, it is a way for myself to improve at development in Node as it seems to be the way forward.

## Where do you lurk?

I stream over at twitch on `twitch.tv/zer0xeon`. Swing by at some point.

### Current NPM Packages in use.

Chalk - For pretty colours in the log.